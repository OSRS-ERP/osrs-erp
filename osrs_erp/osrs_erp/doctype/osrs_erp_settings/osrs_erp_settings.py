# -*- coding: utf-8 -*-
# Copyright (c) 2021, https://gitlab.com/OSRS-ERP and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document

class OSRSERPSettings(Document):
	pass
