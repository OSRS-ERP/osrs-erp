# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in osrs_erp/__init__.py
from osrs_erp import __version__ as version

setup(
	name='osrs_erp',
	version=version,
	description='An ERP implementation for Old School RuneScape',
	author='https://gitlab.com/OSRS-ERP',
	author_email='admin@mystic.dev',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
